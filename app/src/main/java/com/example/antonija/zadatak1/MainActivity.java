package com.example.antonija.zadatak1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.antonija.zadatak1.R;

public class MainActivity extends AppCompatActivity {

    private TextView tvResult;
    private EditText etNumberInput1;
    private EditText etNumberInput2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUI();
    }
    private void initializeUI()
    {
        this.tvResult= (TextView) this.findViewById(R.id.tvResult);
        this.etNumberInput1=(EditText) this.findViewById(R.id.etNumberInput1);
        this.etNumberInput2=(EditText) this.findViewById(R.id.etNumberInput2);
    }
    public void AddNumber(View view)
    {
        String snumber1 = this.etNumberInput1.getText().toString();
        double dnumber1 = Double.parseDouble(snumber1);
        String snumber2 = this.etNumberInput2.getText().toString();
        double dnumber2 = Double.parseDouble(snumber2);
        double add = dnumber1+dnumber2;
        this.tvResult.setText(String.valueOf(add));
    }
    public void SubNumber(View view)
    {
        String snumber1 = this.etNumberInput1.getText().toString();
        double dnumber1 = Double.parseDouble(snumber1);
        String snumber2 = this.etNumberInput2.getText().toString();
        double dnumber2 = Double.parseDouble(snumber2);
        double add = dnumber1-dnumber2;
        this.tvResult.setText(String.valueOf(add));
    }
    public void MultiplyNumber(View view)
    {
        String snumber1 = this.etNumberInput1.getText().toString();
        double dnumber1 = Double.parseDouble(snumber1);
        String snumber2 = this.etNumberInput2.getText().toString();
        double dnumber2 = Double.parseDouble(snumber2);
        double add = dnumber1*dnumber2;
        this.tvResult.setText(String.valueOf(add));
    }
    public void DivideNumber(View view)
    {
        String snumber1 = this.etNumberInput1.getText().toString();
        double dnumber1 = Double.parseDouble(snumber1);
        String snumber2 = this.etNumberInput2.getText().toString();
        double dnumber2 = Double.parseDouble(snumber2);
        double add = dnumber1/dnumber2;
        this.tvResult.setText(String.valueOf(add));
    }
}
